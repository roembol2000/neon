const getIdFromMention = (mention) => {
  // Check for mentions and put the id's in an array
  const possibleMatches = mention.match(/^<@!?(\d+)>$/);

  if (!possibleMatches) return;

  // Returning element 1 instead of 0, as the first element is the entire mention
  return possibleMatches[1];
};

module.exports = (mention, client) => {
  const idFromMention = getIdFromMention(mention);

  if (idFromMention) return client.users.cache.get(idFromMention);

  return client.users.cache.get(mention);
};
