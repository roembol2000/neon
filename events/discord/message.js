const config = require('../../config.json');

module.exports = {
  name: 'message',
  execute(message, client) {
    if (!message.content.startsWith(config.prefix) || message.author.bot)
      return;

    const args = message.content.slice(config.prefix.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();

    const command =
      client.commands.get(commandName) ||
      client.commands.find(
        (cmd) => cmd.aliases && cmd.aliases.includes(commandName)
      );

    if (!command) return;

    if (command.guildOnly && message.channel.type === 'dm') {
      return message.reply(
        'Sorry, but you can only use that command in servers!'
      );
    }

    if (client.cooldowns.get(message.author.id) == command.name) {
      return message.reply(
        'Please wait a moment before you can execute that command again.'
      );
    }

    if (command.permissions) {
      const memberPerms = message.channel.permissionsFor(message.author);
      if (!memberPerms || !memberPerms.has(command.permissions)) {
        return message.channel.send(
          "Whoops! You don't have permission to use this command."
        );
      }
    }

    if (command.args && !args.length) {
      let reply = "You didn't provide any arguments!";

      if (command.usage)
        reply += `\n\`${config.prefix}${commandName} ${command.usage}\``;

      return message.channel.send(reply);
    }

    if (command.cooldown) {
      client.cooldowns.set(message.author.id, command.name);
      setTimeout(() => {
        client.cooldowns.delete(message.author.id);
      }, command.cooldown * 1000);
    }

    try {
      command.execute(message, args);
    } catch (error) {
      console.log(error);
      return message.channel.send(
        'Whoops! There was an issue trying to execute that command...'
      );
    }
  },
};
