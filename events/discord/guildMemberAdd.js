const Canvas = require('canvas');
const Discord = require('discord.js');

const Welcome = require('../../models/Welcome');

// Pass the entire Canvas object because you'll need access to its width and context
const applyText = (canvas, text, size) => {
  const context = canvas.getContext('2d');

  // Declare a base size of the font
  let fontSize = size;

  do {
    // Assign the font to the context and decrement it so it can be measured again
    context.font = `bold ${(fontSize -= 5)}px 'Whitney'`;
    // Compare pixel width of the text to the canvas minus the approximate avatar size
  } while (context.measureText(text).width > canvas.width - 80 - 440);

  // Return the result to use in the actual canvas
  return context.font;
};

module.exports = {
  name: 'guildMemberAdd',
  async execute(member, client) {
    const welcomeSettings = await Welcome.findById(member.guild.id);
    if (!welcomeSettings) return;
    if (!welcomeSettings.enabled) return;

    const channel = member.guild.channels.cache.get(welcomeSettings.channelId);
    if (!channel) return;
    // Create a 1080x450 pixels canvas and get its context
    // The context will be used to modify the canvas
    const canvas = Canvas.createCanvas(1080, 450);
    const context = canvas.getContext('2d');

    // Since the image takes time to load, you should await it
    const background = await Canvas.loadImage('./banner.png');
    // This uses the canvas dimensions to stretch the image onto the entire canvas
    context.drawImage(background, 0, 0, canvas.width, canvas.height);

    // context.fillStyle = '#36393f';
    // context.fillRect(0, 0, canvas.width, canvas.height);

    // Assign the decided font to the canvas
    context.font = applyText(canvas, member.displayName, 100);
    context.fillStyle = '#ffffff';
    context.fillText(
      member.displayName,
      canvas.width / 2.5,
      canvas.height / 1.8
    );

    context.font = applyText(canvas, 'Welcome,', 70);
    context.fillText('Welcome,', canvas.width / 2.5, 155);

    // Pick up the pen
    context.beginPath();
    // Start the arc to form a circle
    context.arc(225, 225, 125, 0, Math.PI * 2, true);
    // Put the pen down
    context.closePath();
    // Clip off the region you drew on
    context.clip();

    // Wait for Canvas to load the image
    const avatar = await Canvas.loadImage(
      member.user.displayAvatarURL({ format: 'jpg' })
    );
    // Draw a shape onto the main canvas
    context.drawImage(avatar, 100, 100, 250, 250);

    // Use the helpful Attachment class structure to process the file for you
    const attachment = new Discord.MessageAttachment(
      canvas.toBuffer(),
      'welcome-image.png'
    );
    channel.send(attachment);
  },
};
