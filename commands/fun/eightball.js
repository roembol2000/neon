const answers = [
  'It is certain.',
  'Without a doubt.',
  'You may rely on it',
  'Yes, definitely',
  'It is decidedly so.',
  'As I see it, yes.',
  'Most likely',
  'Yes.',
  'Outlook good',
  'Signs point so yes.',
  'Reply hazy, try again.',
  'Better not tell you now.',
  'Ask again later.',
  'Cannot predict now.',
  'Concentrate and ask again.',
  "I don't like you right now, ask again later.",
  "Don't count on it.",
  'Outlook not so good.',
  'My sources say no.',
  'Very doubtful.',
  'My reply is no.',
];

module.exports = {
  name: 'eightball',
  description: 'Ask the 8-ball anything!',
  aliases: ['8ball', '8-ball', 'eight-ball'],
  args: true,
  usage: '<question>',
  execute(message, args) {
    const answerIndex = Math.floor(Math.random() * answers.length);
    const answer = answers[answerIndex];
    return message.channel.send(answer);
  },
};
