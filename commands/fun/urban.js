const axios = require('axios');

module.exports = {
  name: 'urban',
  description: 'Search for a term in the urban dictionary!',
  args: true,
  usage: '<query>',
  execute(message, args) {
    const endpointUrl = 'https://api.urbandictionary.com/v0/define';
    const params = new URLSearchParams();
    params.append('term', args.join(' '));

    axios.get(endpointUrl, { params }).then((res) => {
      const result = res.data.list[0];

      if (!result)
        return message.channel.send("Couldn't find any matching items!");

      const reg = /[\[\]]/g;

      const answer = {
        definition: result.definition.replace(reg, ''),
        example: result.example.replace(reg, ''),
        permalink: result.permalink,
        likes: result.thumbs_up,
        dislikes: result.thumbs_down,
        author: result.author,
        written_on: result.written_on,
      };

      console.log('le [baguette] is the [[baguette]]'.replace(reg, ''));

      const embed = {
        title: `Urban definition of '${args.join(' ')}':`,
        url: answer.permalink,
        color: 5814783,
        fields: [
          {
            name: 'Definition',
            value: answer.definition,
          },
          {
            name: 'Example',
            value: answer.example,
          },
          {
            name: '👍',
            value: answer.likes,
            inline: true,
          },
          {
            name: '👎',
            value: answer.dislikes,
            inline: true,
          },
        ],
        footer: {
          text: `By ${answer.author}`,
        },
        timestamp: answer.written_on,
      };

      return message.channel.send({ embed });
    });
  },
};
