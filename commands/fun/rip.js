const Canvas = require('canvas');
const Discord = require('discord.js');

module.exports = {
  name: 'rip',
  description: 'Rest in peace, friend.',
  async execute(message, args) {
    const canvas = Canvas.createCanvas(866, 1300);
    const context = canvas.getContext('2d');

    const background = await Canvas.loadImage('./gravestone.jpg');

    context.drawImage(background, 0, 0, canvas.width, canvas.height);

    const avatar = await Canvas.loadImage(
      message.member.user.displayAvatarURL({ format: 'jpg' })
    );

    context.font = "100px 'Roboto Slab'";
    context.fillStyle = '#555555';
    context.textAlign = 'center';
    context.fillText('RIP', canvas.width / 2, 750);

    context.font = "75px 'Roboto Slab'";
    context.fillText(
      message.author.createdAt.getFullYear() + ' - ' + new Date().getFullYear(),
      canvas.width / 2,
      825
    );

    context.beginPath();
    context.closePath();
    context.arc(433, 480, 150, 0, Math.PI * 2, true);
    context.clip();

    context.drawImage(avatar, 283, 330, 300, 300);

    let imgData = context.getImageData(
      0,
      0,
      context.canvas.width,
      context.canvas.height
    );
    let pixels = imgData.data;
    for (var i = 0; i < pixels.length; i += 4) {
      let lightness = parseInt((pixels[i] + pixels[i + 1] + pixels[i + 2]) / 3);

      pixels[i] = lightness;
      pixels[i + 1] = lightness;
      pixels[i + 2] = lightness;
    }
    context.putImageData(imgData, 0, 0);

    const attachment = new Discord.MessageAttachment(
      canvas.toBuffer(),
      'rip.jpg'
    );
    message.channel.send(attachment);
  },
};
