module.exports = {
  name: 'join',
  description: 'simulate join event',
  execute(message, args) {
    message.client.emit('guildMemberAdd', message.member);
  },
};
