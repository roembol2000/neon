const getUser = require('../../functions/getUser');
const ordinalSuffix = require('../../functions/ordinalSuffix');

const Warn = require('../../models/Warn');

module.exports = {
  name: 'warn',
  description: 'Warn a user',
  guildOnly: true,
  args: true,
  usage: '<user> [reason]',
  permissions: 'MANAGE_MESSAGES',
  async execute(message, args) {
    const user = getUser(args[0], message.client);
    const userToWarn = message.guild.member(user);

    if (!user) return message.channel.send('That user does not exist.');

    let reason = args;
    reason.shift();
    reason = reason.join(' ');

    const warn = new Warn({
      userId: user.id,
      serverId: message.guild.id,
      reason: reason ? reason : '',
      moderator: message.author.id,
    });

    const amountOfWarns = await Warn.find({
      userId: user.id,
      serverId: message.guild.id,
    }).count((err, count) => {
      return count;
    });

    warn
      .save()
      .then((savedDoc) => {
        let embed = {
          title: `Warned ${user.username}`,
          description: `This is their ${ordinalSuffix(
            amountOfWarns + 1
          )} warning.`,
        };

        if (reason) {
          embed.fields = [];
          embed.fields.push({ name: 'Reason', value: reason });
        }

        message.channel.send({ embed });

        embed = {
          title: `You have been warned in ${message.guild.name}`,
          description: `This is your ${ordinalSuffix(
            amountOfWarns + 1
          )} warning.`,
          fields: [
            { name: 'Reason', value: reason ? reason : 'No reason provided.' },
          ],
        };

        user.send({ embed }).catch(() => {
          message.channel.send(
            "I can't dm the user, but they were still warned."
          );
        });
      })
      .catch((err) => {
        console.log(err);
        message.channel.send(
          'There was an issue saving the warn to my database. The issue has been reported.'
        );
      });
  },
};
