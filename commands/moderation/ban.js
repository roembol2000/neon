const getUser = require('../../functions/getUser');

module.exports = {
  name: 'ban',
  description: 'Ban a member from the server',
  args: true,
  usage: '<member> [reason]',
  permissions: 'BAN_MEMBERS',
  guildOnly: true,
  async execute(message, args) {
    const user = getUser(args[0], message.client);
    const userToBan = message.guild.member(user);

    if (!userToBan)
      return message.channel.send(
        "That's and invalid user... Please mention the user you want to ban."
      );

    const authorRolePos = message.member.roles.highest.position;
    const userToBanPos = userToBan.roles.highest.position;

    if (message.author == user)
      return message.channel.send('Please reconsider banning yourself.');

    if (userToBanRolePos >= authorRolePos)
      return message.channel.send(
        "You can't ban people with the same role or higher."
      );

    if (!userToKick.kickable)
      return message.channel.send(
        'I do not have permissions to ban this person.'
      );

    let reason = args;
    reason.shift();
    reason = reason.join(' ');

    banOptions = { days: 1 };
    if (reason) banOptions.reason;

    userToBan.ban(banOptions);

    let embed = {
      title: 'Banned user',
      description: `Banned user ${user.tag}${reason ? ` for ${reason}` : ''}`,
    };

    message.channel.send({ embed });

    embed = {
      title: `You have been banned from ${message.guild.name}`,
      fields: [
        {
          name: 'Reason',
          value: reason ? reason : 'No reason provided',
        },
      ],
    };

    user.send({ embed }).catch(() => {
      message.channel.send("I can't dm the user, but they were still banned.");
    });
  },
};
