const getUser = require('../../functions/getUser');

const Warn = require('../../models/Warn');

module.exports = {
  name: 'warns',
  description: 'See all warns for a user',
  aliases: ['seewarns'],
  guildOnly: true,
  args: true,
  usage: '<user>',
  permissions: 'MANAGE_MESSAGES',
  async execute(message, args) {
    const user = getUser(args[0], message.client);
    const userToSeeWarns = message.guild.member(user);

    if (!user) return message.send('That use does not exist.');

    const authorRolePos = message.member.roles.highest.position;
    const userToSeeWarnsRolePos = userToSeeWarns.roles.highest.position;

    if (userToSeeWarnsRolePos > authorRolePos)
      return message.channel.send(
        "You can't see warns of people with the same role or higher."
      );

    const embed = {
      title: `Warns for ${user.username}`,
    };

    const warns = await Warn.find({
      userId: user.id,
      serverId: message.guild.id,
    });

    if (!warns[0]) return message.channel.send('This user has no warns.');

    embed.fields = [];
    for (warn of warns) {
      embed.fields.push({
        name: 'date goes here',
        value: warn.reason ? warn.reason : 'No reason specified',
        inline: true,
      });
    }

    message.channel.send({ embed });
  },
};
