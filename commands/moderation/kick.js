const getUser = require('../../functions/getUser');

module.exports = {
  name: 'kick',
  description: 'Kick a member from the server',
  args: true,
  usage: '<member> [reason]',
  permissions: 'KICK_MEMBERS',
  guildOnly: true,
  async execute(message, args) {
    const user = getUser(args[0], message.client);
    const userToKick = message.guild.member(user);

    if (!userToKick)
      return message.channel.send(
        "That's an invalid user... Please mention the user you want to kick."
      );

    const authorRolePos = message.member.roles.highest.position;
    const userToKickRolePos = userToKick.roles.highest.position;

    if (message.author == user)
      return message.channel.send("Woah, woah, let's not kick ourselves!");

    if (userToKickRolePos >= authorRolePos)
      return message.channel.send(
        "You can't kick people with the same role or higher."
      );

    if (!userToKick.kickable)
      return message.channel.send(
        'I do not have permissions to kick this person.'
      );

    const embed = {
      title: 'Kicked user',
      description: `Kicked user ${userToKick.tag} for '${reason}'`,
      color: 8472451,
    };

    userToKick
      .kick()
      .then(() => message.channel.send({ embed }))
      .catch(() => message.channel.send(`Unable to kick ${userToKick.tag}!`));
  },
};
