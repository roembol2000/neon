module.exports = {
  name: 'purge',
  description: 'Clear a selected amount of messages.',
  aliases: ['clear', 'clearmessages'],
  guildOnly: true,
  args: true,
  usage: '<amount>',
  permissions: 'MANAGE_MESSAGES',
  async execute(message, args) {
    if (isNaN(args[0])) return message.channel.send('Please provide a number');

    if (!message.guild.me.hasPermission(this.permissions))
      return message.channel.send("I don't have the permissions to do this.");

    if (args[0] > 100)
      return message.channel.send(
        "You can't delete more than 100 messages at a time, due to Discord limitations."
      );

    message.delete();

    message.channel.bulkDelete(Number(args[0]), true).catch((err) => {
      console.log(err);
    });

    const embed = {
      title: 'Cleared messages',
      description: `Removed ${args[0]} messages.`,
    };

    const sentMessage = await message.channel.send({ embed });

    setTimeout(() => {
      sentMessage.delete().catch(console.error);
    }, 3000);
  },
};
