const axios = require('axios');
const { resolve } = require('path');

const Welcome = require('../../models/Welcome');

const imgurEndpoint = 'https://i.imgur.com/';
const style = 'default';

const stringToBool = (string) => {
  if (string == 'enabled') return true;
  if (string == 'disable') return false;
  return;
};

module.exports = {
  name: 'setwelcomebanner',
  description: 'Configure the welcome banner',
  args: true,
  usage: '<state> <channel-id> [wallpaper]',
  permissions: 'MANAGE_GUILD',
  guildOnly: true,
  cooldown: 1,
  execute(message, args) {
    let newData = {};

    if (args.length < 2)
      return message.channel.send(
        `You didn't provide all the neccesary arguments!\nThe neccesary arguments are: \`${this.usage}\``
      );

    const state = stringToBool(args[0]);

    if (state === undefined)
      return message.channel.send(
        'Please specify if the function should be `enabled` or `disabled`.'
      );

    const channel = message.client.channels.cache.get(args[1]);

    if (!channel)
      return message.channel.send(
        "That channel doesn't seem exist, or is from another server. Make sure you use the channel id!"
      );
    if (!(channel.guild == message.guild))
      return message.channel.send(
        'Please provide a channel id from this server.'
      );

    if (args[2]) {
      const imageId = args[2].match(
        /https?:\/\/i\.imgur\.com\/([a-zA-Z0-9]{1,9}\.(?:png|jpg))/
      );

      if (!imageId)
        return message.channel.send(
          'Please provide a valid direct Imgur URL (must be either jpg or png).'
        );

      const test = axios
        .get(imgurEndpoint + imageId[1], { responseType: 'arraybuffer' })
        .then((response) => {
          Welcome.findOneAndUpdate(
            { _id: message.guild.id },
            {
              enabled: state,
              channelId: channel.id.toString(),
              style: style,
              wallpaper: imageId[1],
            },
            { upsert: true },
            (err, doc) => {
              if (err) {
                message.channel.send(
                  'An error has occured! The problem has been reported.'
                );
                return console.log(err);
              }

              return message.channel.send('Successfully changed settings!');
            }
          );
        })
        .catch(() => {
          return message.channel.send(
            'Please provide a valid direct Imgur URL (must be either jpg or png).'
          );
        });
    }
  },
};
