module.exports = {
  name: 'ping',
  description: 'Ping!',
  aliases: ['pong'],
  async execute(message, args) {
    const pingMessage = await message.channel.send('Testing latency...');

    const apiLatency = Math.round(message.client.ws.ping);
    const fullLatency = pingMessage.createdAt - message.createdAt;

    const embed = {
      title: 'Pong! :ping_pong:',
      color: 5814783,
      fields: [
        {
          name: 'API Latency',
          value: `\`${apiLatency}ms\``,
        },
        {
          name: 'Full Latency',
          value: `\`${fullLatency}ms\``,
        },
      ],
    };

    pingMessage.edit({ embed });
  },
};
