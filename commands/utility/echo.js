module.exports = {
  name: 'echo',
  description: "I'll repeat whatevery you say.",
  aliases: ['say'],
  args: true,
  usage: '<message>',
  cooldown: 4,
  execute(message, args) {
    message.channel.send(args.join(' '));
  },
};
