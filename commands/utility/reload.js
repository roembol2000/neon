const fs = require('fs');

module.exports = {
  name: 'reload',
  description: 'Reload a command.',
  args: true,
  usage: '<command>',
  execute(message, args) {
    const commandName = args[0].toLowerCase();
    const command =
      message.client.commands.get(commandName) ||
      message.client.commands.find(
        (cmd) => cmd.aliases && cmd.aliases.includes(commandName)
      );
    if (!command) return message.channel.send('That command does not exist.');

    const commandFolders = fs.readdirSync('./commands');
    const folderName = commandFolders.find((folder) =>
      fs.readdirSync(`./commands/${folder}`).includes(`${commandName}.js`)
    );

    delete require.cache[
      require.resolve(`../${folderName}/${command.name}.js`)
    ];

    try {
      const newCommand = require(`./${command.name}.js`);
      message.client.commands.set(newCommand.name, newCommand);
      message.channel.send(`Successfuly reloaded \`${command.name}\``);
    } catch (error) {
      console.log(error);
      message.channel.send(
        'Whoops! There was an issue reloading the command. Please check the console for more information.'
      );
    }
  },
};
