const fs = require('fs');
const { prefix } = require('../../config.json');

module.exports = {
  name: 'help',
  description: 'Show a menu of all commands available!',
  aliases: ['commands', 'commandlist', 'helper'],
  usage: '[command]',
  execute(message, args) {
    const data = [];
    const commandCategories = fs.readdirSync('./commands');

    if (!args.length) {
      const embed = {
        title: 'Command Categories',
        description: `Use \`${prefix}${this.name} [categories]\` to display every command in a category!`,
        fields: [],
      };

      for (const commandCategory of commandCategories)
        embed.fields.push({
          name: `\`${commandCategory}\``,
          value: '🟥',
        });

      return message.channel.send({ embed });
    }

    if (commandCategories.includes(args[0])) {
      const commands = fs
        .readdirSync(`./commands/${args[0]}`)
        .filter((file) => file.endsWith('.js'));

      const embed = {
        title: `Commands for ${args[0]}`,
        description: `Use \`${prefix}${this.name} [command]\` to display how to use the command!`,
        fields: [],
      };

      for (const command of commands) {
        embed.fields.push({
          name: `\`${command.slice(0, -3)}\``,
          value: message.client.commands.get(command.slice(0, -3)).description,
          inline: true,
        });
      }

      return message.channel.send({ embed });
    }

    const command =
      message.client.commands.get(args[0]) ||
      message.client.commands.find(
        (command) => command.aliases && command.aliases.includes(args[0])
      );

    if (command) {
      const embed = {
        title: `Information for \`${command.name}\``,
        fields: [],
      };

      embed.fields.push({ name: 'Description', value: command.description });

      if (command.aliases)
        embed.fields.push({
          name: 'Aliases',
          value: command.aliases.join(', '),
        });
      if (command.usage)
        embed.fields.push({
          name: 'Usage',
          value: `\`${prefix}${args[0]} ${command.usage}\``,
        });

      if (command.permissions)
        embed.fields.push({
          name: 'Permissions',
          value: `\`${command.permissions}\`\n${
            message.member.hasPermission(command.permissions)
              ? ':white_check_mark: You have permissions to execute this command!'
              : ":no_entry_sign: You don't have permissions to execute this command."
          }`,
        });

      return message.channel.send({ embed });
    }

    return message.channel.send("That command doesn't exist.");
  },
};
