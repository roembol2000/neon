const fs = require('fs');
const Discord = require('discord.js');
const mongoose = require('mongoose');

const config = require('./config.json');

require('dotenv').config();

const client = new Discord.Client();

mongoose.connect(process.env.MONGOURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;

client.commands = new Discord.Collection();
client.cooldowns = new Discord.Collection();

const discordEventFiles = fs
  .readdirSync('./events/discord')
  .filter((file) => file.endsWith('.js'));
const mongoEventFiles = fs
  .readdirSync('./events/mongo')
  .filter((file) => file.endsWith('.js'));
const commandFolders = fs.readdirSync('./commands');

for (const file of discordEventFiles) {
  const event = require(`./events/discord/${file}`);

  if (event.once) {
    client.once(event.name, (...args) => event.execute(...args, client));
  } else {
    client.on(event.name, (...args) => event.execute(...args, client));
  }
}

for (const file of mongoEventFiles) {
  const event = require(`./events/mongo/${file}`);

  if (event.once) {
    db.once(event.name, () => event.execute());
  } else {
    db.on(event.name, () => event.execute());
  }
}

for (const folder of commandFolders) {
  const commandFiles = fs
    .readdirSync(`./commands/${folder}`)
    .filter((file) => file.endsWith('.js'));

  for (const file of commandFiles) {
    const command = require(`./commands/${folder}/${file}`);
    client.commands.set(command.name, command);
  }
}

client.login(process.env.TOKEN);
