const { Schema, model } = require('mongoose');

const WarnSchema = new Schema({
  userId: String,
  serverId: String,
  reason: String,
  moderator: String,
});

module.exports = model('Warn', WarnSchema);
