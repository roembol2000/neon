const { Schema, model } = require('mongoose');

const WelcomeSchema = new Schema({
  _id: Number,
  enabled: Boolean,
  channelId: String,
  style: String,
});

module.exports = model('Welcome', WelcomeSchema);
